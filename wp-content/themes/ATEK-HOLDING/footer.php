		<?php
		if ( is_front_page() ) :
			get_template_part( 'part', 'home' );
		endif;
		?>
		<?php get_template_part( 'part', 'service' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>