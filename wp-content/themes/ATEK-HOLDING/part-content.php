<?php
if ( is_front_page() ) :
	get_template_part( 'part', 'banner-home' );
endif;
if ( is_front_page() ) :
	$style = 'grey';
else :
	$style = '';
endif;
if ( is_page( 'quienes-somos' ) ) :
	get_template_part( 'part', 'banner-about-us' );
endif;
if ( is_page( 'contactenos-2' ) ) :
	get_template_part( 'part', 'banner-contact-us' );
endif;
?>
<!-- Begin Content -->
	<section class="content <?php echo $style; ?> wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->