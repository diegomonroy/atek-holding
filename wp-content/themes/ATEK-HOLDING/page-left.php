<?php

/*

Template Name: Left

*/

?>
<?php get_header(); ?>
<!-- Begin Left Wrap -->
	<section class="left_wrap wow fadeIn" data-wow-delay="0.5s">
		<div class="show-for-small-only">
			<button type="button" class="hollow button" data-toggle="offCanvas">Servicios</button>
			<div class="off-canvas-wrapper">
				<div class="off-canvas-absolute position-left" id="offCanvas" data-off-canvas>
					<button type="button" class="close-button" aria-label="Cerrar" data-close><span aria-hidden="true">&times;</span></button>
					<h3 class="menu">NUESTROS SERVICIOS</h3>
					<?php
					wp_nav_menu(
						array(
							'menu_class' => 'vertical menu',
							'container' => false,
							'theme_location' => 'left-menu',
							'items_wrap' => '<ul class="%2$s">%3$s</ul>'
						)
					);
					?>
					<?php get_template_part( 'part', 'left' ); ?>
				</div>
				<div class="off-canvas-content" data-off-canvas-content>
					<div class="row">
						<div class="small-12 columns">
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<?php the_content(); ?>
							<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="hide-for-small-only">
			<div class="row">
				<div class="medium-3 columns">
					<h3 class="menu">NUESTROS SERVICIOS</h3>
					<?php
					wp_nav_menu(
						array(
							'menu_class' => 'vertical menu',
							'container' => false,
							'theme_location' => 'left-menu',
							'items_wrap' => '<ul class="%2$s">%3$s</ul>'
						)
					);
					?>
					<?php get_template_part( 'part', 'left' ); ?>
				</div>
				<div class="medium-9 columns">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Left Wrap -->
<?php get_footer(); ?>