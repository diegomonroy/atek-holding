<!-- Begin Home -->
	<section class="home wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home' ); ?>
			</div>
		</div>
	</section>
<!-- End Home -->