<!-- Begin Left -->
	<section class="left wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'left' ); ?>
			</div>
		</div>
	</section>
<!-- End Left -->